import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'theme/:id',
    loadChildren: () => import('./pages/theme/theme.module').then( m => m.ThemePageModule)
  },
  {
    path: 'article/:id',
    loadChildren: () => import('./pages/article/article.module').then( m => m.ArticlePageModule)
  },
  {
    path: 'courses/:id',
    loadChildren: () => import('./pages/courses/courses.module').then( m => m.CoursesPageModule)
  },
  {
    path: 'topic/:id',
    loadChildren: () => import('./pages/topic/topic.module').then( m => m.TopicPageModule)
  },
  {
    path: 'books/:id',
    loadChildren: () => import('./pages/books/books.module').then( m => m.BooksPageModule)
  },
  {
    path: 'chapter/:id',
    loadChildren: () => import('./pages/chapter/chapter.module').then( m => m.ChapterPageModule)
  },
  {
    path: 'questions',
    loadChildren: () => import('./pages/questions/questions.module').then( m => m.QuestionsPageModule)
  },
  {
    path: 'languages',
    loadChildren: () => import('./pages/languages/languages.module').then( m => m.LanguagesPageModule)
  },
  {
    path: 'search',
    loadChildren: () => import('./pages/search/search.module').then( m => m.SearchPageModule)
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  },
  {
    path: 'complement',
    loadChildren: () => import('./pages/complement/complement.module').then( m => m.ComplementPageModule)
  },
  {
    path: 'singular-book',
    loadChildren: () => import('./pages/singular-book/singular-book.module').then( m => m.SingularBookPageModule)
  },
  //{ path: '**', redirectTo: ''},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
