import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Plugins, StatusBarStyle } from '@capacitor/core';
import { TranslateService } from '@ngx-translate/core';
import { AppConfigService } from './services/app-config.service';
const { SplashScreen, StatusBar } = Plugins;


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  public loading = true;
  constructor(
    private platform: Platform,
    private translate: TranslateService,
    private appConfig: AppConfigService
  ) {
    this.initializeApp();
  }

  async initializeApp() {
    this.platform.ready().then( async () => {
      if (this.platform.is('android')) {
        StatusBar.setStyle({
          style: StatusBarStyle.Dark
        }).catch( e => {
          console.warn(e);
        });
      }else{
        StatusBar.setStyle({
          style: StatusBarStyle.Light
        }).catch( e => {
          console.warn(e);
        });
      }
      await this.translateConfig();
      console.log('HIDING screen');
      SplashScreen.hide();
    });
  }
  async translateConfig() {
    let browserLang = this.translate.getBrowserLang();
    browserLang = /(es|en|ru|he)/gi.test(browserLang) ? browserLang : 'es';
    console.log('userlang', browserLang);
    this.appConfig.currentLang = browserLang;
    let lang = await this.appConfig.getLanguage();
    if (!lang) {
      lang = browserLang;
      this.translate.setDefaultLang('es');
    }
    console.log('setted lang', lang);
    await this.translate.use(lang).toPromise<void>();
    console.log('transalated');    
    await this.appConfig.setLanguage(lang);
    
  }
}
