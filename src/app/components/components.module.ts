import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { FooterComponent } from './footer/footer.component';
//import { PipesModule } from '../pipes/pipes.module';
import { HasbaraLoaderComponent } from './hasbara-loader/hasbara-loader.component';


@NgModule({
    declarations: [
        HasbaraLoaderComponent,
        FooterComponent
    ],
    imports: [IonicModule, CommonModule, FormsModule, TranslateModule],
    exports: [
        FooterComponent,
        HasbaraLoaderComponent,
    ]
})
export class ComponentsModule { }