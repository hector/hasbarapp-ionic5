import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.page.html',
  styleUrls: ['./article.page.scss'],
})
export class ArticlePage implements OnInit {
  articleId = null;
  article = null;
  questions = [];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(queryParams => {
      if(this.articleId) return;
      this.articleId = queryParams.get('id');
      this.getArticle();
      this.getArticleQuestions()
    });
  }
  ionViewWillEnter(){
    
  }

  getArticle(){
    this.dataService.getArticle(this.articleId).subscribe(async (res: any) => {
      this.article = res;
    });
  }

  getArticleQuestions(){
    this.dataService.getQuestions(this.articleId).subscribe(async (res: any) => {
      console.log(res);
      this.questions = res;
    });
  }

  openAnswers(questionNode: any){
    let navigationExtras: NavigationExtras = {
      state: {
        question: questionNode
      }
    };
    this.router.navigate(['questions'], navigationExtras);
  }

}
