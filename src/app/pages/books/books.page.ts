import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppConfigService } from 'src/app/services/app-config.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.page.html',
  styleUrls: ['./books.page.scss'],
})
export class BooksPage implements OnInit {
  bookId = null;
  book = null;
  chapters = [];
  currentLang = 'es';
  constructor(
    private dataService: DataService,
    private route: ActivatedRoute,
    private appConfig: AppConfigService
  ) { }
  ionViewWillEnter() {
    this.currentLang = this.appConfig.currentLang;
  }
  ngOnInit() {
    this.route.paramMap.subscribe(queryParams => {
      this.bookId = queryParams.get('id');
      this.getBook();
      this.getChapters();
    });
  }

  getBook(){
    this.dataService.getBook(this.bookId).subscribe(async (res: any) => {
      this.book = res;
    });
  }

  getChapters(){
    this.dataService.getBookChapters(this.bookId).subscribe(async (res: any) => {
      console.log(res);
      this.chapters = res;
    });
  }

}
