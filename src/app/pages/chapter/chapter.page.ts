import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-chapter',
  templateUrl: './chapter.page.html',
  styleUrls: ['./chapter.page.scss'],
})
export class ChapterPage implements OnInit {
  chapterId = null;
  chapter = null;
  txt: SafeHtml;
  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(queryParams => {
      this.chapterId = queryParams.get('id');
      this.getTopic();
    });
  }

  getTopic(){
    this.dataService.getChapter(this.chapterId).subscribe(async (res: any) => {
      console.log(res);
      this.chapter = res;
      this.txt = this.sanitizer.bypassSecurityTrustHtml(this.chapter?.content?.rendered);
    });
  }
}
