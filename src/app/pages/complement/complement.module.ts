import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ComplementPageRoutingModule } from './complement-routing.module';

import { ComplementPage } from './complement.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    ComplementPageRoutingModule
  ],
  declarations: [ComplementPage]
})
export class ComplementPageModule {}
