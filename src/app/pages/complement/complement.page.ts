import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { AlertController, LoadingController, ModalController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-complement',
  templateUrl: './complement.page.html',
  styleUrls: ['./complement.page.scss'],
})
export class ComplementPage implements OnInit {
  dataForm: any = null;
  submitting = false;
  @Input() question: any;
  constructor(
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private platform: Platform,
    private alertController: AlertController,
    private dataServide: DataService,
    private translate: TranslateService,
    private loadingController: LoadingController,
  ) {}

  ngOnInit() {
    console.log(this.question.question.question.rendered);
    this._buildForm();
  }

  _buildForm(){
    this.dataForm = this.formBuilder.group({
      name: new FormControl('', [
        Validators.required
      ]),
      email: new FormControl('', [
        Validators.required,
        this.emailValidator
      ]),
      message: new FormControl('', [
        Validators.required
      ])
    });
  }
/*------------------------------------------------------------*/
  get name() { return this.dataForm.get('name'); }
  get email() { return this.dataForm.get('email'); }
  get message() { return this.dataForm.get('message'); }

  dismiss(data: any = {}) {
    this.modalController.dismiss();
  }
  emailValidator(control: any) {
    const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    if (!EMAIL_REGEXP.test(control.value)) {
      return {invalidEmail: true};
    }
  }

  async sendAnswer(){
    let data = {...(this.dataForm.value)};
    data.question = this.question?.question?.question?.rendered ?? 'Sin pregunta';
    console.log(data);
    this.submitting = true;
    const texts =  this.translate.instant([
      'MESSAGE_SEND',
      'MESSAGE_NOT_SEND',
      'OK'
    ]);
    let message = null;
    if (this.platform.is('ios')){
      message = texts.ADDING_CANDLE;
    }
    const loading = await this.loadingController.create({
      backdropDismiss: false,
      message
    });
    await loading.present();
    await this.dataServide.sendAnwser(data).then(
      async (result) => {
        console.log(result);
        loading.dismiss();
        this.submitting = false;
        if(!result){          
          await this.presentAlert('',texts.MESSAGE_NOT_SEND, texts.OK);
        }else{
          await this.presentAlert('', texts.MESSAGE_SEND, texts.OK);
          this.dismiss({
            buyed: true
          });
        }
      },async (error) =>{
        this.submitting = false;
        loading.dismiss();
        await this.presentAlert('',texts.MESSAGE_NOT_SEND, texts.OK);
      });
  }
  async presentAlert(header: string, message: string, button: string) {
    const alert = await this.alertController.create({
      header,
      message,
      backdropDismiss: false,
      buttons: [button]
    });
    await alert.present();
  }
}
