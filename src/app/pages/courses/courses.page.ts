import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.page.html',
  styleUrls: ['./courses.page.scss'],
})
export class CoursesPage implements OnInit {
  courseId = null;
  course = null;
  topics = [];
  constructor(
    private dataService: DataService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(queryParams => {
      this.courseId = queryParams.get('id');
      this.getCourseContent();
      this.getTopics();
    });
  }

  getCourseContent(){
    this.dataService.getCourse(this.courseId).subscribe(async (res: any) => {
      this.course = res;
    });
  }

  getTopics(){
    this.dataService.getCourseTopics(this.course).subscribe(async (res: any) => {
      this.topics = res;
    });
  }
}
