import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  themes = [];
  courses = [];
  books = [];
  searchValue = '';
  constructor(
    private dataService: DataService,
    private router: Router
  ) {}

  ngOnInit() {
    /*this.dataService.getCourses().subscribe(async (res: any) => {
      this.courses = res;
    });
*/
    this.dataService.getBooks().subscribe(async (res: any) => {
      this.books = res;
    });
    
  }
  ionViewWillEnter(){
    this.dataService.getThemes().subscribe(async (res: any) => {
      this.themes = res;
    });
  }
  sendForm(){
    if(this.searchValue == '') return;
    let navigationExtras: NavigationExtras = {
      queryParams: {
        search: this.searchValue
      }
    };
    this.router.navigate(['search'], navigationExtras);
  }
}
