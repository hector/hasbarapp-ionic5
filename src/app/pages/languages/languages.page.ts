import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.page.html',
  styleUrls: ['./languages.page.scss'],
})
export class LanguagesPage implements OnInit {
  currentLang: any = null;  
  constructor(
    private translateService: TranslateService,
    private appConfig: AppConfigService,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.currentLang = this.appConfig.currentLang;
  }

  async changeLanguage() {
    const loading = await this.loadingController.create({
      backdropDismiss: true
    });
    await loading.present();
    this.translateService.use(this.currentLang).subscribe((event: any) => {
      console.log('new lang-> ',this.currentLang);
      this.appConfig.setLanguage(this.currentLang);
      loading.dismiss();
    });
  }
}
