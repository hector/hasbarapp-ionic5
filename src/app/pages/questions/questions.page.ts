import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { Plugins } from '@capacitor/core';
import { TranslateService } from '@ngx-translate/core';
import { ModalController, ToastController } from '@ionic/angular';
import { ComplementPage } from '../complement/complement.page';
const { Share, Clipboard } = Plugins;


@Component({
  selector: 'app-questions',
  templateUrl: './questions.page.html',
  styleUrls: ['./questions.page.scss'],
})
export class QuestionsPage implements OnInit {
  question:any = null;
  questionTypes = [];
  questionTones = [];
  currentType = 'all';
  selectedTones = [];
  selectedTone = '';
  toneColor={
    "contundente-rojo": "danger",
    "amigable-verde": "success",
    "normal-amarillo": "warning"
  }
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService,
    private ref: ChangeDetectorRef,
    private translate: TranslateService,
    private toastController: ToastController,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    if(this.router.getCurrentNavigation().extras.state){
      this.question = this.router.getCurrentNavigation().extras.state;
      this.fillTypes(this.question?.question?.answers);
      this.fillTones(this.question?.question?.answers);
    } 
  }

  fillTypes(answers: any){
    let types = [];
    answers.forEach((el: any) => {
      if(el?.type?.slug){
        types.push({
          type: el?.type?.slug ?? 'sin-tipo',
          text: el?.type?.title?.rendered ?? 'Sin tipo'
        });
      }
    });
    this.questionTypes = types.reduce((unique, o) => {
        if(!unique.some(obj => obj.type === o.type && obj.text === o.text)) {
          unique.push(o);
        }
        return unique;
      },[]);
  }

  fillTones(answers: any){
    let tones = [];
    answers.forEach((el: any) => {
      if(el?.tone?.slug){
        tones.push({
          type: el?.tone?.slug ?? 'sin-tono',
          text: el?.tone?.title?.rendered ?? 'Sin tono'
        });
      }
    });
    console.log(tones);
    this.questionTones = tones.reduce((unique, o) => {
      if(!unique.some(obj => obj.type === o.type && obj.text === o.text)) {
        unique.push(o);
      }
      return unique;
    },[]);
    console.log(this.questionTones);
   // this.selectedTones = this.questionTones;
   this.questionTones.forEach((item) => {
    this.selectedTones.push(item.type);
   });
  }


  getTypeDesc(){
    let ct = this.questionTypes.filter(el => {
      return el.type == this.currentType;
    });
    if(ct.length == 0) return 'Todos';
    return ct[0]['text'];
  }

  unselectTone(tone: any){
    console.log(tone);
    if(tone == this.selectedTone){
      console.log('iguales');
      this.selectedTone = '';
      this.selectedTones = ['contundente-rojo','amigable-verde','normal-amarillo'];
    }else{
      this.selectedTone = tone;
      this.selectedTones = [tone];
    }
    this.ref.detectChanges();
  }

  async shareSocialMedia(){
    console.log(this.question?.question?.question);
    const q = this.question?.question?.question;
    const texts =  this.translate.instant([
      'SHARE_DIALOG',
      'SHARE_QUESTION'
    ]);
    let shareRet = await Share.share({
      title: texts['SHARE_QUESTION'],
      text: q?.rendered,
      url: q?.permalink,
      dialogTitle: texts['SHARE_DIALOG']
    });
  }

  async shareAnswer(answer: any){
    console.log(answer);
    console.log(this.question?.question?.question);
    const q = this.question?.question?.question;
    const texts =  this.translate.instant([
      'SHARE_DIALOG',
      'SHARE_QUESTION'
    ]);
    let shortAns = answer?.short_answer;

    const strippedString = shortAns.replace(/(<([^>]+)>)/gi, "");
    let shareRet = await Share.share({
      title: q?.rendered,
      text: q?.rendered + ': ' + strippedString,
      url: q?.permalink,
      dialogTitle: texts['SHARE_DIALOG']
    });
  }

  copyAnswer(answer: any){
    let shortAns = answer?.short_answer;
    const strippedString = shortAns.replace(/(<([^>]+)>)/gi, "");
    const texts =  this.translate.instant([
      'COPIED_CLIPBOARD',
    ]);
    Clipboard.write({
      string: strippedString,
    }).then( async () =>{
      const toast = await this.toastController.create({
        message: texts['COPIED_CLIPBOARD'],
        duration: 2000
      });
      toast.present();
    });
  }

  async complement(anwser: any){
    const modal = await this.modalController.create({
      component: ComplementPage,
      componentProps: {
        'question': this.question
      }
    });
    return await modal.present();
  }

}
