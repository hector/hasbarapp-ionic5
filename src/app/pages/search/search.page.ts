import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  searchText = '';
  results = [];
  loaded = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(queryParams => {
      console.log(queryParams);
      if(queryParams && queryParams.search){
        this.searchText = queryParams.search;
        this.getResults();
      }else{
        this.router.navigate(['']);
      }
    });
  }

  getResults(){
    this.loaded = false;
    this.dataService.searchPosts(this.searchText).subscribe(async (res: any) => {
      console.log(res);
      this.results = res;
      this.loaded = true;
    });
  }

  openResult(result: any){
    if(result.subtype == 'article'){
      this.router.navigate(['article', result.id]);
    }
    if(result.subtype == 'chapter'){
      this.router.navigate(['chapter', result.id]);
    }
    if(result.subtype == 'topic'){
      this.router.navigate(['topic', result.id]);
    }
    return false;
  }

}
