import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SingularBookPage } from './singular-book.page';

const routes: Routes = [
  {
    path: '',
    component: SingularBookPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SingularBookPageRoutingModule {}
