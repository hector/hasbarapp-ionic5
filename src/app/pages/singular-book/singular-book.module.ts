import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SingularBookPageRoutingModule } from './singular-book-routing.module';

import { SingularBookPage } from './singular-book.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    TranslateModule.forChild(),
    SingularBookPageRoutingModule
  ],
  declarations: [SingularBookPage]
})
export class SingularBookPageModule {}
