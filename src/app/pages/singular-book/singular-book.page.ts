import { Component, OnInit } from '@angular/core';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-singular-book',
  templateUrl: './singular-book.page.html',
  styleUrls: ['./singular-book.page.scss'],
})
export class SingularBookPage implements OnInit {
  currentLang = 'es';
  bookLink = '';
  bookLinkEn = 'https://www.amazon.com/300-Questions-Words-Realities-Israeli-Palestinian/dp/B08F6JZ51K/';
  bookLinkEs = 'https://www.amazon.com/300-Preguntas-Palabras-realidades-conflicto/dp/9878501442/'
  constructor(private appConfig: AppConfigService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.currentLang = this.appConfig.currentLang;
    this.bookLink = this.bookLinkEn;
    if(this.currentLang == 'es'){
      this.bookLink = this.bookLinkEs;
    }
  }

}
