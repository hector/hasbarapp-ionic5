import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ThemePageRoutingModule } from './theme-routing.module';

import { ThemePage } from './theme.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ThemePageRoutingModule
  ],
  declarations: [ThemePage]
})
export class ThemePageModule {}
