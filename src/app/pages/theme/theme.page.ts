import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-theme',
  templateUrl: './theme.page.html',
  styleUrls: ['./theme.page.scss'],
})
export class ThemePage implements OnInit {
  themeId = null;
  theme = null;
  articles = [];
  constructor(
    private dataService: DataService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(queryParams => {
      if(this.themeId) return;
      this.themeId = queryParams.get('id');
      this.getThemeContent();
      this.getArticles();
    });
  }

  getThemeContent(){
    this.dataService.getTheme(this.themeId).subscribe(async (res: any) => {
      console.log(res);
      this.theme = res;
    });
  }

  getArticles(){
    this.dataService.getThemeArticles(this.themeId).subscribe(async (res: any) => {
      console.log(res);
      this.articles = res;
    });
  }

}
