import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.page.html',
  styleUrls: ['./topic.page.scss'],
})
export class TopicPage implements OnInit {
  topicId = null;
  topic = null;
  txt: SafeHtml;
  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(queryParams => {
      this.topicId = queryParams.get('id');
      this.getTopic();
    });
  }

  getTopic(){
    this.dataService.getTopic(this.topicId).subscribe(async (res: any) => {
      console.log(res);
      this.topic = res;
      this.txt = this.sanitizer.bypassSecurityTrustHtml(this.topic?.content?.rendered);
    });
  }

}
