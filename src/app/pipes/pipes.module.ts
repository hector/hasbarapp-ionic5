import { NgModule } from '@angular/core';
import { QuestionsPipe, QuestionsTonePipe } from './questions.pipe';
@NgModule({
    declarations: [QuestionsPipe, QuestionsTonePipe],
    imports: [],
    exports: [QuestionsPipe, QuestionsTonePipe]
})
export class PipesModule {

}