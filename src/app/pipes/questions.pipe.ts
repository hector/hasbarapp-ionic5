import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'questions'
})
export class QuestionsPipe implements PipeTransform {

  transform(items: unknown[], ...args: unknown[]) {
    if(args.indexOf('all') > -1){
      return items;
    }
    let type = args[0];
    return items.filter((item: any) => {
      if(item?.type?.slug){
        return item.type.slug === type;
      }
      return false;
    });
  }
}


@Pipe({
  name: 'questionsTone',
  pure: false
})
export class QuestionsTonePipe implements PipeTransform {

  transform(items: unknown[], ...args: any[]) {
    let tones = args[0];
    return items.filter((item: any) => {
      if(item?.tone?.slug){    
        return (tones.indexOf(item.tone.slug) > -1);
      }
      return false;
    });
  }
}