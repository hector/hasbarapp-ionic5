import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { Platform } from '@ionic/angular';
const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  public currentLang = 'es';
  constructor(public platform: Platform) { }
  async setLanguage(lang = 'es') {
    await Storage.set({
      key: 'lang',
      value: lang
    });
    this.currentLang = lang;
  }

  async getLanguage() {
    const { value } = await Storage.get({ key: 'lang' });
    console.log('Got lang from storage: ', value);
    return value;
  }
}