import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AlertController } from '@ionic/angular';
import { map, tap, catchError } from 'rxjs/operators';
import { AppConfigService } from './app-config.service';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private http: HttpClient,
    private appConfig: AppConfigService,
    // private plt: Platform,
    private alertController: AlertController
  ) { }

  getLang(){
    const lang = this.appConfig.currentLang;
    if(lang == 'es'){
      return '';
    }
    return lang + '/';
  }

  /*--------------------------------------------------------------------------*/
  showAlert(msg: any) {
    const alert = this.alertController.create({
      message: msg,
      buttons: ['OK']
    });
    alert.then((res: any) => {
      return res.present();
    });
  }
  /*--------------------------------------------------------------------------*/
  getThemes() {
    return this.http.get(environment.baseUrl + this.getLang() + environment.apiPath + 'theme')
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to HasbarApp API');
          throw new Error(e);
        })
      );
  }
  /*--------------------------------------------------------------------------*/
  getTheme(themeId: any) {
    return this.http.get(environment.baseUrl + this.getLang() + environment.apiPath + 'theme/' + themeId)
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to HasbarApp API');
          throw new Error(e);
        })
      );
  }
  /*--------------------------------------------------------------------------*/
  getThemeArticles(themeId: any) {
    const params = new HttpParams()
      .set('theme', themeId);
    return this.http.get(environment.baseUrl + this.getLang() + environment.apiPath + 'article', { params })
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to HasbarApp API');
          throw new Error(e);
        })
      );
  }
  /*--------------------------------------------------------------------------*/
  getArticle(articleId: any) {
    return this.http.get(environment.baseUrl + this.getLang() + environment.apiPath + 'article/' + articleId)
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to HasbarApp API');
          throw new Error(e);
        })
      );
  }
  /*--------------------------------------------------------------------------*/
  getCourses() {
    return this.http.get(environment.baseUrl + this.getLang() + environment.apiPath + 'course')
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to HasbarApp API');
          throw new Error(e);
        })
      );
  }
  /*--------------------------------------------------------------------------*/
  getCourse(courseId: any) {
    return this.http.get(environment.baseUrl + this.getLang() + environment.apiPath + 'course/' + courseId)
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to HasbarApp API');
          throw new Error(e);
        })
      );
  }
  /*--------------------------------------------------------------------------*/
    getCourseTopics(courseId: any) {
      const params = new HttpParams()
      .set('course', courseId);
      return this.http.get(environment.baseUrl + this.getLang() + environment.apiPath + 'topic')
        .pipe(
          tap((res: any) => {
            return res;
          }),
          catchError(e => {
            console.log(e);
            this.showAlert('Cannot connect to HasbarApp API');
            throw new Error(e);
          })
        );
    }
  /*--------------------------------------------------------------------------*/
  getTopic(topicId: any) {
    return this.http.get(environment.baseUrl + this.getLang() + environment.apiPath + 'topic/' + topicId)
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to HasbarApp API');
          throw new Error(e);
        })
      );
  }
  /*--------------------------------------------------------------------------*/
  getBooks() {
    return this.http.get(environment.baseUrl + this.getLang() + environment.apiPath + 'book')
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to HasbarApp API');
          throw new Error(e);
        })
      );
  }
  /*--------------------------------------------------------------------------*/
  getBook(bookId: any) {
    return this.http.get(environment.baseUrl + this.getLang() + environment.apiPath + 'book/' + bookId)
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to HasbarApp API');
          throw new Error(e);
        })
      );
  }
  /*--------------------------------------------------------------------------*/
    getBookChapters(bookId: any) {
      const params = new HttpParams()
      .set('book', bookId);
      return this.http.get(environment.baseUrl + this.getLang() + environment.apiPath + 'chapter')
        .pipe(
          tap((res: any) => {
            return res;
          }),
          catchError(e => {
            console.log(e);
            this.showAlert('Cannot connect to HasbarApp API');
            throw new Error(e);
          })
        );
    }
  /*--------------------------------------------------------------------------*/
  getChapter(topicId: any) {
    return this.http.get(environment.baseUrl + this.getLang() + environment.apiPath + 'chapter/' + topicId)
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to HasbarApp API');
          throw new Error(e);
        })
      );
  }

  /*--------------------------------------------------------------------------*/
  getQuestions(articleId: any) {
    return this.http.get(environment.baseUrl + this.getLang() + environment.apiPathv1 + 'questions/' + articleId)
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to HasbarApp API');
          throw new Error(e);
        })
      );
  }
  /*--------------------------------------------------------------------------*/
  searchPosts(search: any) {
    const params = new HttpParams()
      .set('search', search)
      .set('type', 'post')
      .set('subtype', 'topic,chapter,article')
      ;
    return this.http.get(environment.baseUrl + this.getLang() + environment.apiPath + 'search/',{params})
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to HasbarApp API');
          throw new Error(e);
        })
      );
  }

  async sendAnwser(data: any){

    let formData: any = new FormData();
    formData.append('question', data.question);
    formData.append('your-name', data.name);
    formData.append('your-email', data.email);
    formData.append('your-message', data.message);
    //const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });

    return this.http.post(environment.baseUrl + this.getLang() + environment.apiFormUrl + 'contact-forms/8967/feedback', formData)
    .toPromise()
    .then((res: any) => {
        return res?.posted_data_hash || 0;
    }, (e) => {
      console.log(e);
      this.showAlert('Cannot connect to HasbarApp API');
      throw new Error(e);
    });
  }


}//class




