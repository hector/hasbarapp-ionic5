export const environment = {
  production: true,
  baseUrl: "https://screen.com.ve/clientes/hasbarapp/",
  apiPath: "wp-json/wp/v2/",
  apiPathv1: "wp-json/hasbarapp/v1/",
  apiFormUrl: "wp-json/contact-form-7/v1/"
};